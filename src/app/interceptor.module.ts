import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
// import { SessionService } from './services/session.service';
// import { TranslatorService } from './shared/services/translator.service';

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // let authRequest = request;
    const token = localStorage.getItem('accessToken');
    if (token) {
        request = request.clone({ headers: request.headers.set('access-token', token) });
        request = request.clone({ headers: request.headers.set('expiry', localStorage.getItem('expiry')) });
        request = request.clone({ headers: request.headers.set('token-type', 'Bearer') });
        request = request.clone({ headers: request.headers.set('uid', localStorage.getItem('uid')) });
        request = request.clone({ headers: request.headers.set('client', localStorage.getItem('client')) });
    }

    return next.handle(request);

  }
};
