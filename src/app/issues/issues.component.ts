import { Component, OnInit, ViewChild } from '@angular/core';
import * as Chartist from 'chartist';
import { Angular2TokenService, SignInData } from 'angular2-token';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import * as _ from 'lodash';

@Component({
  selector: 'app-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.css']
})
export class IssuesComponent implements OnInit {
  
  constructor() { }

  ngOnInit() { }

}
