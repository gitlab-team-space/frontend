import { Injectable, EventEmitter, Output, OnInit } from '@angular/core';
import { User } from '../models/user';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

@Injectable()
export class UserService {
  private user: User;

  @Output() userChangeEvent: EventEmitter<User> = new EventEmitter(true);

  constructor ( private localStorage: LocalStorageService) {}

  setUser(user: User) {
    this.user = user;
    this.localStorage.store('user', this.user);
    this.userChangeEvent.emit(user);
  }

  destroyUser() {
    this.localStorage.clear('user');
    this.userChangeEvent.emit(null);
  }

  getUser(): User {
    this.user = this.localStorage.retrieve('user');
    return this.user;
  }
  

}
