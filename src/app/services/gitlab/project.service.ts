import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Project } from '../../models/project';
import { UserService } from '../user.service';

import * as _ from 'lodash';

@Injectable()
export class GitlabProjectService {

  constructor (private http: HttpClient, private userService: UserService) {}

  myProjects (params?: any): Observable<Project[]> {
    if (!_.isObject(params)) {
      params = {};
    }

    params['page'] = params['page'] || 1;
    params['per_page'] = params['per_page'] || 5;  

    const currentUser = this.userService.getUser();
    
    return this.http.get<Project[]>('/gitlab/users/' + currentUser.nickname + '/projects', {params: this.parseParams(params)});
  }

  search (params?: any): Observable<Project[]> {
    if (!_.isObject(params)) {
      params = {search: params};
    }
    
    params['page'] = params['page'] || 1;
    params['per_page'] = params['per_page'] || 20;  

    const currentUser = this.userService.getUser();

    return this.http.get<Project[]>('/gitlab/projects', {params: this.parseParams(params)});
  }

  parseParams(params: any): HttpParams {
    const httpParams = new HttpParams({
        fromObject: params
    });
    return httpParams;
}

}
