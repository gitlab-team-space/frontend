import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Project } from '../models/project';
import * as _ from 'lodash';

@Injectable()
export class ProjectService {

  constructor (private http: HttpClient ) {}

  list (params?: any): Observable<Project[]> {
    if (!_.isObject(params)) {
      params = {};
    }

    params['page'] = params['page'] || 1;
    params['per_page'] = params['per_page'] || 5;  
    return this.http.get<Project[]>('/api/projects/');
  }

}
