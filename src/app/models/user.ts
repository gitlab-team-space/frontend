export class User {
    id: number;
    email: string;
    password: string;
    passwordConfirmation: string;
    login: string;
    nickname: string;
    name: string;
    
}
