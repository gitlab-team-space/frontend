import { Component, OnInit, ViewChild } from '@angular/core';
import { Angular2TokenService, SignInData } from 'angular2-token';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import * as _ from 'lodash';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private user: User;
  signInData: SignInData = <SignInData>{};
  @ViewChild('emailErrors') emailErrors;
  @ViewChild('passwordErrors') passwordErrors;

  constructor(private _tokenService: Angular2TokenService, 
    private userService: UserService, private router: Router) {
      this.userService.userChangeEvent.subscribe(user => {
        this.router.navigate(['/dashboard']);
      });
    }

  ngOnInit() {
    if (this.userService.getUser()) {
      this.router.navigate(['/dashboard']);
    }
  }

  signIn() {
    this._tokenService.signIn(this.signInData).subscribe(
        response => {          
            const user = <User>response.json().data;
            this.userService.setUser(user);
            this.router.navigateByUrl('/');
        },
        errors => {
          this.handleError(errors);
        }
    );
  }

  handleError(error: any){
    const errors  = _.isObject(error.json()) ? error.json().errors : JSON.parse(error.error);
    if(!errors['email'] && !errors['password']){
      this.passwordErrors.setErrors(errors);
    }
    this.emailErrors.setErrors(errors['email']);
    this.passwordErrors.setErrors(errors['password']);
  }

}
