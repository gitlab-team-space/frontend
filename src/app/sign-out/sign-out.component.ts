import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Angular2TokenService } from 'angular2-token';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { User } from '../models/user';

@Component({
    selector: 'sign-out',
    template: '',
})
export class SignOutComponent implements OnInit {

    user: User;

    constructor(private _tokenService: Angular2TokenService, private router: Router, 
        private userService: UserService) { }

    ngOnInit() {
        this._tokenService.signOut().subscribe(
            response => {
              this.userService.destroyUser();
              localStorage.removeItem('accessToken');
              localStorage.removeItem('client');
              localStorage.removeItem('expiry');
              localStorage.removeItem('tokenType');
              localStorage.removeItem('uid');
              this.router.navigate(['']);
            }, error => {
                console.log(error);
                this.router.navigate(['']);
            },
        );
    }
}
